#include "utils.hpp"
#include <algorithm>

using namespace std;

void import_data(vector<vector<double>> &data, string in_file, unsigned neglect_first, unsigned &n_columns, unsigned &n_rows)
{
    ifstream  f(in_file);
    string    s;

    // Ignoro la riga con il commento e conto le colonne
    getline(f, s);
    getline(f, s);
    n_columns = count(s.begin(), s.end(), '\t');
    f.close();

    char                        tmp_str[12*n_columns];
    vector<vector<double>>      v;

    FILE* input_file = fopen(in_file.c_str(), "r");

    // Ignoro la riga con il commento e il transiente iniziale
    for (unsigned i=0; i<neglect_first+1; i++)
        fgets(tmp_str, 12*n_columns, input_file);

    for (n_rows=0; fgets(tmp_str, 12*n_columns, input_file) != NULL; n_rows++)
        v.push_back(split(tmp_str, n_columns));

    fclose(input_file);

    data.resize(n_columns);
    for (unsigned i=0; i<n_columns; i++)
    {
        data[i].resize(n_rows);
        for (unsigned j = 0; j < n_rows; j++)
            data[i][j] = v[j][i];
    }
}

vector<double> split (char* line, unsigned cols)
{
    vector<double>      toReturn(cols, 0);
    char*               cusu = strtok(line, "\t");

    for (unsigned i=0; i<cols; i++)
    {
        toReturn[i] = atof(cusu);
        cusu = strtok(NULL, "\t");
    }
    return toReturn;
}

YAML::Node get_params(string filename)
{
    ifstream file(filename);
    string yml;
    getline(file, yml);
    yml = yml.substr(1, string::npos);
    replace(yml.begin(), yml.end(), ',', '\n');
    return YAML::Load(yml);
}
