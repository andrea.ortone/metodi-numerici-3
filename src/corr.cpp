#include "utils.hpp"

using namespace std;

inline double f (double x)
{
    return x;
}

double sample_mean(vector<double> &v, unsigned size)
{
    long double sum = 0;
    for(unsigned i=0; i<size; i++)
        sum += f(v[i]);
    return sum/size;
}

int main (int argc, char* argv[])
{
    string   in_file, out_file;
    unsigned Te, Tmc, neglect_first, mode;
    double   toll;

    in_file  = argv[1];
    out_file = argv[2];
    mode     = atoi(argv[3]);
    // mode  = 0 for correlation in Tmc
    // mode  = 1 for correlation in Te
    // mode  = 2 for correlation in Te separately for each Tmc

    toll = 1.e-5;
    neglect_first = 20; // rows to be neglected due to termalization

    FILE                    *output_file;
    vector<vector<double>>  v;

    import_data(v, in_file, neglect_first, Te, Tmc);

    // v is Te x Tmc shaped: v[i] is a MC story at a fixed Euclidean Time

    cout << "Te = " << Te << "\tTmc = " << Tmc << endl;

    vector<double>   f_mean(Te, 0);
    double           sum;

    output_file = fopen(out_file.c_str(), "w");
    switch(mode)
    {
    case 0: // correlation in Tmc for every Te
        for (unsigned i=0; i<Te; i++)
            f_mean[i] = sample_mean(v[i], Tmc);

        for (unsigned k=0; k<Tmc-1; k++) {
            for (unsigned j=0; j<Te; j++) {
                sum = 0;
                for (unsigned i=0; i<Tmc-k; i++)
                    sum += (f(v[j][i]) - f_mean[j])*(f(v[j][i+k]) - f_mean[j]);

                fprintf(output_file, "%lf\t", sum/(Tmc-k-1));
            }
            fprintf(output_file, "\n");
        }
        break;

    case 1: // correlation in Te for every starting Te
        for (unsigned i=0; i<Te; i++)
            f_mean[i] = sample_mean(v[i], Tmc);

        for (unsigned k=0; k<unsigned(Te/5); k++) {
            for (unsigned j=0; j<Te; j++) {
                sum = 0;
                for (unsigned i=0; i<Tmc; i++)
                    sum += (f(v[j][i]) - f_mean[j]) * (f(v[(j+k)%Te][i]) - f_mean[(j+k)%Te]); // Uso la periodicità del tempo euclideo

                fprintf(output_file, "%lf\t", sum/(Tmc-1));
            }
            fprintf(output_file, "\n");
        }
        break;

    case 2: // correlation in Te separately for each Tmc
        double mean;
        for (unsigned k=0; k<unsigned(Te/2); k++)
        {
            for (unsigned j=0; j<Tmc; j++)
            {
                sum = 0;
                mean = 0;

                for(unsigned i=0; i<Te; i++)
                    mean += f(v[i][j]);

                for(unsigned i=0; i<Te; i++)
                    sum += f(v[i][j])*f(v[(i+k)%Te][j]);

                fprintf(output_file, "%lf\t", sum/Te - pow(mean/Te, 2));
            }
            fprintf(output_file, "\n");
        }
        break;
    }

    fclose(output_file);
}
