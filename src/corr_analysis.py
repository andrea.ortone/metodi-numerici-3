#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from sys import argv
import glob
from utils import filename_to_params, blocking


plt.style.use('style.yml')


def fit(x, A, B):
    return A*np.exp(-B*x)


files = sorted(
    glob.glob(f'{argv[1]}/corr*.txt'),
    key=lambda x: tuple(np.array(filename_to_params(x))[[2,1,0]])  # sorta con nu -> eta -> N
)


nus = []
delta10s = []
delta10_errs = []
etas = []


nu_tmp = filename_to_params(files[0])[2]
delta10s_tmp = []
delta10_errs_tmp = []
etas_tmp = []

for f_index, f in enumerate(files):
    """ Legge i file e assumendo temperatura abbastanza bassa, per ognuno
    produce una coppia (eta, gap10) """
    print(f'Faccio file {f_index}, {f}')
    N, eta, nu = filename_to_params(f)

    if (abs(nu_tmp - nu) > 1e-5):
        nus.append(nu_tmp)
        delta10s.append(delta10s_tmp)
        delta10_errs.append(delta10_errs_tmp)
        etas.append(etas_tmp)

        delta10s_tmp = []
        delta10_errs_tmp = []
        etas_tmp = []
        nu_tmp = nu

    data = np.loadtxt(f)

    ks = list(range(len(data)))
    Cs = []
    C_errs = []

    for k, row in enumerate(data):
        Cs.append(row.mean())

        # find blocksize
        chosen_blockSize = int(N/20)
        # blockSizes = range(1, int(len(data[0])/2), 2)
        # stds = []

        # for blockSize in blockSizes:
        #     stds.append(blocking(row, blockSize))

        # plt.plot(blockSizes, stds)
        # plt.plot(2*[chosen_blockSize], [min(stds), max(stds)])
        # plt.title(f'N = {N}, eta = {eta}, nu = {nu}, k = {k}')
        # plt.show()

        # scelgo un blocksize che va bene sempre
        C_errs.append(blocking(row, chosen_blockSize))

    # Faccio il fit
    opt, cov = curve_fit(fit, ks, Cs, sigma=C_errs, absolute_sigma=True)

    kmin = round(2/(5*eta)) + 1
    kmax = round(3/opt[1])
    gap_k = max(1, round((kmax-kmin)/8))

    print(f'fit eseguito su {len(ks[kmin:kmax:gap_k])} dati, gap_k = {gap_k}, {(kmax-kmin)/8}')

    # opt, cov = curve_fit(fit, ks[kmin:kmax], Cs[kmin:kmax], sigma=C_errs[kmin:kmax], absolute_sigma=True)
    opt, cov = curve_fit(fit, ks[kmin:kmax:gap_k], Cs[kmin:kmax:gap_k], sigma=C_errs[kmin:kmax:gap_k], p0=opt, absolute_sigma=True)

    # plt.errorbar(ks, Cs, yerr=C_errs, marker='x', linestyle='')
    # X = np.linspace(min(ks), max(ks), 1000)
    # plt.plot(X, fit(X, *opt))
    # plt.plot(2*[kmin], [0, Cs[0]])
    # plt.plot(2*[kmax], [0, Cs[0]])
    # plt.yscale('log')
    # plt.xlim(kmin, kmax)
    # plt.ylim(Cs[kmax], Cs[kmin])
    # plt.title(f'N = {N}, eta = {eta}, nu = {nu}')
    # plt.show()

    # calcolo il gap e l'errore
    delta10s_tmp.append(opt[1]/eta)
    delta10_errs_tmp.append(np.sqrt(cov[1][1])/eta)
    etas_tmp.append(eta)

    if f_index == len(files)-1:
        nus.append(nu_tmp)
        delta10s.append(delta10s_tmp)
        delta10_errs.append(delta10_errs_tmp)
        etas.append(etas_tmp)


def quartic(x, A, B, C):
    return -A*(x**2) + B + C*(x**4)


gap10_extrs = []
gap10_extr_errs = []


gap10_v = [1.6749696241, 1.5553128924, 1.4500210200, 1.3560831238, 1.2713393247, 0.7918184039]

# Gap in funzione di eta per i vari nu
for i in range(len(nus)):
    # faccio l'estrapolazione nel limite del continuo
    opt, cov = curve_fit(quartic, etas[i], delta10s[i], sigma=delta10_errs[i], absolute_sigma=True)

    gap10_extrs.append(opt[1])
    gap10_extr_errs.append(np.sqrt(cov[1][1]))

    # grafici
    plt.plot([0, etas[i][-1]], 2*[gap10_v[i]])
    plt.errorbar(np.array(etas[i]), delta10s[i], yerr=delta10_errs[i], label=nus[i])
    plt.errorbar([0], [opt[1]], yerr=[np.sqrt(cov[1][1])])
    X = np.linspace(0, max(etas[i]), 1000)
    plt.plot(X, quartic(X, *opt))
    plt.xlabel(r'$ \eta $')
    plt.ylabel(r'$ E_1 - E_0 $')
    plt.title(rf'$ \nu $ = {nus[i]}')
    plt.savefig(f'gap/{nus[i]}.pdf')
    plt.show()


# Gap al variare di nu
plt.errorbar(nus, gap10_extrs, yerr=gap10_extr_errs, linestyle='', marker='x')
plt.show()

with open('gaps.txt', 'w') as outFile:
    for i in range(len(nus)):
        outFile.write(f'{nus[i]} {gap10_extrs[i]} {gap10_extr_errs[i]}\n')
