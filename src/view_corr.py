#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
from sys import argv
from scipy.optimize import curve_fit


corr = np.loadtxt(argv[1], unpack=True)

print(len(corr))
for i in range(100): # range(0, len(corr), int(len(corr)/100)):
    plt.plot(np.arange(corr.shape[1]), corr[i])
plt.show()


ks = np.arange(corr.shape[1])
corrs = [corr[:,k].mean() for k in range(corr.shape[1])]

def fit(x, A, B):
    return A*np.exp(-B*x)

quanti = int(len(ks)/1)
opt, cov = curve_fit(fit, ks[:quanti], corrs[:quanti])
X = np.linspace(0, quanti, 1000)
plt.plot(X, fit(X, *opt))
plt.plot(ks[:quanti], corrs[:quanti])
plt.show()
print(1/opt[1])
#plt.savefig('corr.png')
