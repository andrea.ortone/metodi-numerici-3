import numpy as np
import re


def filename_to_params(s):
    match = re.match('.*-([0-9]+)-([0-9\.]+)-([0-9\.]+)\.txt', s)
    if not match:
        raise Exception('AAAAA')

    return (
        int(float(match.group(1))), # N
        float(match.group(2)), # eta
        float(match.group(3)) # nu
    )


def stdMean(data: np.array) -> float:
    return data.std(ddof=1)/np.sqrt(len(data))


def blocking(data, blockSize):
    Nblocks = int(len(data)/blockSize)

    blocks = [
        data[i*blockSize: (i+1)*blockSize]
        for i in range(Nblocks)
    ]

    avgs = np.array([
        b.mean()
        for b in blocks
    ])

    return stdMean(avgs) if Nblocks > 1 else 0
