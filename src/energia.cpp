#include "utils.hpp"


int main(int argc, char *argv[])
{
    string   in_file, out_file;
    unsigned Te, Tmc, neglect_first;
    vector<vector<double>>  v;
    double nu, eta;

    in_file = argv[1];
    out_file = argv[2];
    neglect_first = 0;

    auto params = get_params(in_file);
    nu = params["nu"].as<double>();
    eta = params["eta"].as<double>();

    import_data(v, in_file, neglect_first, Te, Tmc);
    cout << "Te = " << Te << "\tTmc = " << Tmc << "\t" << endl;

    // VIRIALE
    // calcolo l'energia separatamente per ogni tempo euclideo
    valarray<double> energy_virial (Te);
    double sum;

    for (unsigned j=0; j<Te; j++)
    {
        sum = 0;
        for(unsigned k=0; k<Tmc; k++)
            sum += (v[j][k]*v[j][k] - 1)*(3*v[j][k]*v[j][k] - 1);
        energy_virial[j] = sum;
    }

    energy_virial *= nu/Tmc;


    // DERIVATA DI log(Z)
    valarray<double> energy_internal (Te);

    for (unsigned j=0; j<Te; j++)
    {
        sum = 0;
        for(unsigned k=0; k<Tmc; k++)
            sum += -pow(v[(j+1)%Te][k] - v[j][k], 2)/(2*eta*eta) + pow(v[j][k]*v[j][k] - 1, 2);
        energy_internal[j] = sum;
    }

    energy_internal = energy_internal*nu/Tmc + 1/(2*eta);

    // Scrivo su file
    FILE* f = fopen(out_file.c_str(), "w");

    for (unsigned i=0; i<Te; i++)
        fprintf(f, "%f %f\n", energy_virial[i], energy_internal[i]);

    fclose(f);

    return 0;
}
