#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
from sys import argv


def stdMean(data: np.array) -> float:
    return data.std(ddof=1)/np.sqrt(len(data))


def blocking(data: np.array, blockSize: int) -> (float, float):
    Nblocks = int(len(data)/blockSize)

    blocks = [
        data[i*blockSize: (i+1)*blockSize]
        for i in range(Nblocks)
    ]

    avgs = np.array([
        b.mean()
        for b in blocks
    ])

    return (
        avgs.mean(),
        stdMean(avgs) if Nblocks > 1 else 0
    )


data = np.loadtxt(argv[1])   # data[k][Tmc]

ks = list(range(data.shape[0]))
means = [data[k].mean() for k in ks]
stds = []

blockSizes = [1000] if False else range(1, int(data.shape[1]/2), 1)

for k in ks:
    stds = []
    print(f'k = {k}')
    for blockSize in blockSizes:
        avg, std = blocking(data[k], blockSize)
        stds.append(std)
        print(f'{blockSize}: {avg:.5f} ± {std:.5f}')

    plt.title(f'k = {k}')
    plt.plot(blockSizes, stds)
    plt.show()


# plt.errorbar(ks, means, yerr=stds)
# plt.show()
