#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from sys import argv

y = np.loadtxt(argv[1])
iter = y.shape[0]
path_lenght = y.shape[1]

x = np.arange(path_lenght+1)

y = np.column_stack((y, y[:,0]))

fig = plt.figure()
ax = plt.axes(ylim=(-5, 5), xlim=(0, max(x)))
points, = ax.plot([], [])
txt = ax.text(x.size/10, 1.5, '', size=50)

def init():
    points.set_data(x, y[0])
    return points, txt

def animate(i):
    txt.set_text(f'{i+1}/{y.shape[0]}')
    points.set_data(x, y[i])
    return points, txt

anim_duration = 10
framerate = 25

anim = animation.FuncAnimation(
    fig, animate, init_func=init, repeat=False,
    frames=range(0, iter, int(iter/(framerate*anim_duration))),
    interval=1000/framerate, blit=True
)

# plt.show()
anim.save('anim.mp4')

# thermalization
def f(x):
    return x;

k = int(path_lenght/10)
if k==0:
    for i in range(path_lenght):
        plt.plot(np.arange(iter), f(y[:, i]))
else:
    for i in range(10):
        plt.plot(np.arange(iter), f(y[:, i*k]))

# plt.show()
plt.savefig('cammini.png')
