#!/usr/bin/env python3

from glob import glob
from sys import argv
from subprocess import run
from utils import filename_to_params
import re
import numpy as np
import matplotlib.pyplot as plt

plt.style.use('style.yml')
basedir = argv[1]


# GAP
# chosen_blockSize = int(N/10) in corr_analysis.py
dirs = sorted(glob(f'{argv[1]}/corr*.dir'))

betas = []
nus = []
gaps = []
gap_errs = []

for d in dirs:
    match = re.match('.*-([0-9]+)\.tar\.gz\.dir', d)
    betas.append(float(match.group(1)))
    run(['./corr_analysis.py', d])
    nu, gap, gap_err = np.loadtxt('gaps.txt', unpack=True)
    nus.append(nu)
    gaps.append(gap)
    gap_errs.append(gap_err)

# Questo è beta-fissato-6-pcg hardcodato perché sono una bestia
betas.append(100)
gaps.append(1.4480308650051577)
gap_errs.append(0.0030124163775223376)

plt.errorbar(betas, gaps, yerr=gap_errs)
plt.plot([min(betas), max(betas)], 2*[1.4500210200]) # per nu = 0.5
plt.xlabel(r'$ \beta $')
plt.ylabel(r'$ E_1 - E_0 $')
plt.savefig('gap-beta.pdf')
plt.show()


# ENERGIA
# chosen_blockSize = int(N/20) in energy_analysis.py
dirs = sorted(glob(f'{argv[1]}/e*.dir'))

betas = []
nus = []
vir_E0s = []
vir_E0_errs = []
der_E0s = []
der_E0_errs = []

for d in dirs:
    match = re.match('.*-([0-9]+)\.tar\.gz\.dir', d)
    betas.append(float(match.group(1)))
    run(['./energy_analysis.py', d])
    nu, vir_E0, vir_E0_err, der_E0, der_E0_err = np.loadtxt('E0s.txt', unpack=True)
    nus.append(nu)
    vir_E0s.append(vir_E0)
    vir_E0_errs.append(vir_E0_err)
    der_E0s.append(der_E0)
    der_E0_errs.append(der_E0_err)

# Questo è beta-fissato-6-pcg hardcodato perché sono una bestia
betas.append(100)
vir_E0s.append(0.7938491159268993)
vir_E0_errs.append(0.0011700788697577118)
der_E0s.append(0.7923353634547596)
der_E0_errs.append(0.0017507394779030746)

plt.errorbar(betas, vir_E0s, yerr=vir_E0_errs, label='viriale')
plt.errorbar(betas, der_E0s, yerr=der_E0_errs, label=r'der $ \log(Z) $')
plt.plot([min(betas), max(betas)], 2*[0.7944652367655844]) # per nu = 0.5
plt.xlabel(r'$ \beta $')
plt.ylabel(r'$ E_0 $')
plt.legend()
plt.savefig('e0-beta.pdf')
plt.show()
