#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import sys
from glob import glob
from utils import filename_to_params

plt.style.use('style.yml')
path = sys.argv[1]

files = glob(f'{path}/mc-2000-*.txt')
print(files)

for f in files:
    data = np.loadtxt(f).flatten()
    plt.hist(
        data, bins=500, density=True
    )
    nu = filename_to_params(f)[2]
    plt.title(rf'$ \nu = {nu} $')
    plt.xlabel('$ x $')
    plt.ylabel(r'$ |\psi|^2 $')
    plt.savefig(f'wavefunction-{nu}.pdf')
    plt.close()

files = sorted(glob(f'{path}/mc-*1.txt'), key=lambda x: filename_to_params(x)[1])
print(files)

for f in files:
    data = np.loadtxt(f).flatten()
    eta = filename_to_params(f)[1]
    plt.hist(
        data, bins=500, density=True, histtype='step',
        label=f'{eta:.2f}'
    )

plt.legend(loc='lower right')
plt.xlabel('$ x $')
plt.ylabel(r'$ |\psi|^2 $')
plt.savefig('wavefunction-etas.pdf')
plt.show()
