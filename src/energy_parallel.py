#!/usr/bin/env python3

from subprocess import run
import threading, queue
import time
import glob
from sys import argv

files = sorted(glob.glob(f'{argv[1]}/mc*.txt'))

q = queue.Queue()

def worker():
    while True:
        item = q.get()
        cmd = ['./energia', item, item.replace('mc', 'e')]
        print(*cmd)
        run(cmd)
        q.task_done()

for f in files:
    q.put(f)

for i in range(10):
    threading.Thread(target=worker, daemon=True).start()

q.join()
