#include <iostream>
#include <random>
#include <yaml-cpp/yaml.h>
#include "pcg-cpp/include/pcg_random.hpp"

using namespace std;

int main ()
{
    // Leggo i parametri
    YAML::Node config = YAML::LoadFile("config.yaml");

    string   outPath   = config["path"]["out"].as<string>();

    double      eta    = config["phys"]["eta"].as<double>();
    unsigned    N      = config["phys"]["N"].as<double>();
    double      nu     = config["phys"]["nu"].as<double>();

    unsigned iter      = config["sim"]["iterazioni"].as<unsigned>(); // sweep
    unsigned save_step = config["sim"]["saveStep"].as<unsigned>(); // ogni quanto misurare
    double   init      = config["sim"]["init"].as<double>();


    double delta = sqrt(2*eta/nu);

    double                             y[N];

    pcg_extras::seed_seq_from<std::random_device> seed_source;
    pcg32 gen(seed_source);

    uniform_real_distribution<double>  dis(-1,1);
    uniform_real_distribution<double>  dis01(0,1);
    unsigned                           acc      = 0;
    double                             yp;
    FILE                              *out_file = fopen(outPath.c_str(), "w");

    fprintf(out_file, "# eta: %f, N: %d, nu: %f, save_step: %d \n", eta, N, nu, save_step);

    // Inizializzo la catena
    for (unsigned i=0; i<N; i++)
        y[i] = init;

    // Faccio il montecarlo
    for (unsigned i=0; i<iter; i++)
    {
        for (unsigned j=0; j<N; j++) // Sweep
        {
            yp = y[j] + delta*dis(gen);
            if (log(dis01(gen))<
                nu * (eta*(pow(y[j],4)-pow(yp,4)) + (2*eta - 1/eta)*(yp*yp - y[j]*y[j]) + (yp-y[j])*(y[(j-1+N)%N]+y[(j+1)%N])/eta))
            {
                y[j] = yp;
                acc++;
            }
        }
        if (!(i%save_step))
        {
            // Salvo le posizioni della catena
            for (unsigned k=0; k<N; k++)
                fprintf(out_file, "%f\t", y[k]);
            fprintf(out_file, "\n");
        }
    }

    fclose(out_file);
    cout << "acceptance rate: " << double(acc)/(((unsigned long)iter)*N) << endl;

    return 0;
}
