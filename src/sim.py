#!/usr/bin/env python3

import numpy as np
from subprocess import run
import threading, queue
import os
import time


name = 'beta-fissato-1'
beta = 100
nus = np.linspace(0.05, 1, 10)
_etas = np.linspace(0.2, 0.8, 6)


Ns = np.floor(beta/_etas)
etas = beta/Ns
if len(Ns) != len(np.unique(Ns)):
    print(Ns)
    raise Exception('Ci sono più N eguali')

q = queue.Queue()

def worker():
    while True:
        item = q.get()
        with open('config.yaml', 'w') as f:
            f.write(item)
        print('========================')
        print(item)
        print('========================')
        run('./cammini_gen')
        q.task_done()

# os.mkdir(name)

for N in Ns:
    for nu in nus:
        eta = beta/N
        out = f'''
path:
  out: {name}/mc-{int(N)}-{eta}-{nu}.txt

phys:
  eta: {eta}
  N:   {int(N)}
  nu:  {nu}

sim:
  iterazioni: {int(1e7/eta)}
  saveStep: {int(1e3/eta)}
  init: 0
'''
        q.put(out)

for i in range(15):
    threading.Thread(target=worker, daemon=True).start()
    time.sleep(2)

q.join()
