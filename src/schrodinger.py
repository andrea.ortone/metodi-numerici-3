#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy import sparse
from scipy.sparse import linalg


def eigen(xmin, xmax, N, Vfun, params, neigs):
    x = np.linspace(xmin, xmax, N)
    dx = x[1] - x[0]

    H = sparse.eye(N, N, format='lil') * 2

    for i in range(N - 1):
        H[i, i + 1] = -1
        H[i + 1, i] = -1

    H /= 2*(dx ** 2)

    V = Vfun(x, params)
    for i in range(N):
        H[i, i] += V[i]
    H = H.tocsc()

    eigenvalues, eigenvectors = linalg.eigs(H, k=neigs, which='SM')

    for i in range(neigs):
        eigenvalues = np.real(eigenvalues)

    return eigenvalues


def VfunHO(x, omega):
    return (0.5 * (omega[0] * x)**2)


def VfunDW(x, params):  # a, b
    return (params[1]*(x**2 - params[0]**2)**2)


plt.style.use('style.yml')

xmin = -10
xmax = 10
N = 2000
a = 1

# calcolo tanti nu
# nus = np.linspace(0.38, 1.02, 125)
# bs = nus**2

# outFile = open('gaps-s.txt', 'w')
# for b in bs:
#     Ec = a*np.sqrt(b)
#     energie = sorted(eigen(xmin, xmax, N, VfunDW, [1, b], 4)/Ec)
#     gap = energie[1]-energie[0]
#     gap2 = energie[3]-energie[0]
#     outFile.write(f'{np.sqrt(b)*a**3:.5f} {energie[0]} {energie[1]} {energie[2]} {energie[3]}\n')
#     print(f'nu = {np.sqrt(b)*a**3:.5f}\t{energie}\t{gap:.10f}\t{gap2:.10f}')
# outFile.close()

# carico i tanti nu calcolati
nus, E0s, E1s = np.loadtxt('gaps-s.txt', unpack=True, usecols=(0, 1, 2))

# plot dei valori ottenuti dal montecarlo
# gap
nu_mcs, gap_mcs, gap_mc_errs = np.loadtxt('gaps.txt', unpack=True)

plt.plot(nus[nus < 0.62], E1s[nus < 0.62] -
         E0s[nus < 0.62], '-', label='Schrödinger')
plt.errorbar(
    nu_mcs, gap_mcs, yerr=gap_mc_errs,
    marker='', linestyle='', label='Monte Carlo'
)
plt.xlabel(r'$ \nu $')
plt.ylabel('$ E_1 - E_0 $')
plt.legend()
plt.savefig('schroed-gap.pdf')
plt.show()

# E0
nu_mcs, vir_E0s, vir_E0_errs, der_E0s, der_E0_errs = np.loadtxt(
    'E0s.txt', unpack=True)
plt.plot(nus, E0s, '-', label='Schrödinger')
plt.errorbar(
    nu_mcs, vir_E0s, yerr=vir_E0_errs,
    marker='x', linestyle='', label='Viriale'
)
plt.errorbar(
    nu_mcs, der_E0s, yerr=der_E0_errs,
    marker='x', linestyle='', label=r'Der $ \log(Z) $'
)
plt.xlabel(r'$ \nu $')
plt.ylabel('$ E_0 $')
plt.legend()
plt.savefig('schroed-e0.pdf')
plt.show()


# calcolo le cose per i nu del montecarlo
nus = np.array([0.4, 0.45, 0.5, 0.55, 0.6, 1])
bs = nus**2

gaps = []

for b in bs:
    Ec = a*np.sqrt(b)
    energie = sorted(eigen(xmin, xmax, N, VfunDW, [1, b], 4)/Ec)
    gap = energie[1]-energie[0]
    gap2 = energie[3]-energie[0]
    print(f'nu = {np.sqrt(b)*a**3:.5f}\t{energie}\t{gap:.10f}\t{gap2:.10f}')
    gaps.append(gap)

print((gap_mcs-gaps)/gap_mc_errs)
