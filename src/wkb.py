#!/usr/bin/env python3

from scipy.integrate import quad
import matplotlib.pyplot as plt
import numpy as np


def gap_wkb(x):
    return np.sqrt(8)/np.pi*np.exp(
        -np.sqrt(8)*x*quad(
            lambda y: np.sqrt((y**2 - 1)**2 - 2**(1/2)/x),
            0, np.sqrt(1 - 2**(1/4) / x**(1/2))
        )[0]
    )


plt.style.use('style.yml')

nus, E0s, E1s = np.loadtxt('gaps-s-ext.txt', unpack=True, usecols=(0, 1, 2))

gap_s = E1s - E0s
cutoff = 13

plt.plot(nus, gap_s, label='Schrödinger')

X = nus[cutoff:]
wkb = [gap_wkb(x) for x in X]
plt.plot(X, wkb, label='WKB')

plt.xlim(0)
plt.yscale('log')
plt.xlabel(r'$ \nu $')
plt.ylabel('$ E_1 - E_0 $')
plt.legend()
plt.savefig('wkb.pdf')
plt.show()

plt.plot(nus[cutoff:], abs(wkb - gap_s[cutoff:])/gap_s[cutoff:])
plt.xlim(0)
plt.ylim(0)
plt.xlabel(r'$ \nu $')
plt.ylabel(r'$ |\Delta_{\mathrm{WKB}}-\Delta_{\mathrm{S}}|/\Delta_{\mathrm{S}} $')
plt.savefig('wkb-err.pdf')
plt.show()
