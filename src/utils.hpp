#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <valarray>
#include <string.h>
#include "utils.hpp"
#include <yaml-cpp/yaml.h>

using namespace std;

vector<double> split (char* line, unsigned cols);
void import_data (vector<vector<double>> &vec, string in_file, unsigned neglect_first, unsigned &n_columns, unsigned &n_rows);
YAML::Node get_params(string filename);

#endif /* UTILS_H */
