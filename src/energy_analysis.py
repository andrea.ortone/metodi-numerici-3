#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from sys import argv
from utils import filename_to_params, blocking
import glob


plt.style.use('style.yml')


files = sorted(
    glob.glob(f'{argv[1]}/e*.txt'),
    key=lambda x: tuple(np.array(filename_to_params(x))[[2,1,0]])  # sorta con nu -> eta -> N
)


nus = []
vir_E0s = []
vir_E0_errs = []
der_E0s = []
der_E0_errs = []
etas = []

nu_tmp = filename_to_params(files[0])[2]
vir_E0s_tmp = []
vir_E0_errs_tmp = []
der_E0s_tmp = []
der_E0_errs_tmp = []
etas_tmp = []

for f_index, f in enumerate(files):
    """ Legge i file e assumendo temperatura abbastanza bassa, per ognuno
    produce una coppia (eta, gap10) """
    print(f'Faccio file {f_index}, {f}')
    N, eta, nu = filename_to_params(f)

    if (abs(nu_tmp - nu) > 1e-5):
        nus.append(nu_tmp)
        vir_E0s.append(vir_E0s_tmp)
        vir_E0_errs.append(vir_E0_errs_tmp)
        der_E0s.append(der_E0s_tmp)
        der_E0_errs.append(der_E0_errs_tmp)
        etas.append(etas_tmp)

        vir_E0s_tmp = []
        vir_E0_errs_tmp = []
        der_E0s_tmp = []
        der_E0_errs_tmp = []
        etas_tmp = []
        nu_tmp = nu

    virial, derZ = np.loadtxt(f, unpack=True)
    etas_tmp.append(eta)

    # VIRIALE
    vir_E0s_tmp.append(virial.mean())

    # find blocksize
    chosen_blockSize = int(N/20)
    # blockSizes = range(1, int(len(virial)/5), 1)
    # stds = []

    # for blockSize in blockSizes:
    #     stds.append(blocking(virial, blockSize))

    # plt.plot(blockSizes, stds)
    # plt.plot(2*[chosen_blockSize], [min(stds), max(stds)])
    # plt.title(f'Viriale N = {N}, eta = {eta}, nu = {nu}')
    # plt.show()

    # scelgo un blocksize che va bene sempre
    vir_E0_errs_tmp.append(blocking(virial, chosen_blockSize))


    # DERIVATA DI log(Z)
    der_E0s_tmp.append(derZ.mean())

    # find blocksize
    chosen_blockSize = int(N/20)
    # blockSizes = range(1, int(len(derZ)/5), 1)
    # stds = []

    # for blockSize in blockSizes:
    #     stds.append(blocking(derZ, blockSize))

    # plt.plot(blockSizes, stds)
    # plt.plot(2*[chosen_blockSize], [min(stds), max(stds)])
    # plt.title(f'log(Z) N = {N}, eta = {eta}, nu = {nu}')
    # plt.show()

    # scelgo un blocksize che va bene sempre
    der_E0_errs_tmp.append(blocking(derZ, chosen_blockSize))


    if f_index == len(files)-1:
        nus.append(nu_tmp)
        vir_E0s.append(vir_E0s_tmp)
        vir_E0_errs.append(vir_E0_errs_tmp)
        der_E0s.append(der_E0s_tmp)
        der_E0_errs.append(der_E0_errs_tmp)
        etas.append(etas_tmp)


def quadratic(x, A, B, C):
    return -A*(x**2) + B + C*(x**4)


E0_v = [0.8134409404234694, 0.8011336576880322, 0.7944652367655844, 0.7921732445232003, 0.7933431759626269, 0.8695731116090761]
vir_E0_extrs = []
vir_E0_extr_errs = []

for i in range(len(nus)):
    # faccio l'estrapolazione nel limite del continuo
    opt, cov = curve_fit(quadratic, etas[i], vir_E0s[i], sigma=vir_E0_errs[i], absolute_sigma=True, p0=[1, 0.8, 1])

    vir_E0_extrs.append(opt[1])
    vir_E0_extr_errs.append(np.sqrt(cov[1][1]))

    # grafici
    plt.plot([0, etas[i][-1]], 2*[E0_v[i]])

    plt.errorbar(np.array(etas[i]), vir_E0s[i], yerr=vir_E0_errs[i], label=nus[i])
    plt.errorbar([0], [opt[1]], yerr=[np.sqrt(cov[1][1])])
    X = np.linspace(0, max(etas[i]), 1000)
    plt.plot(X, quadratic(X, *opt))
    plt.xlabel(r'$ \eta $')
    plt.ylabel('$ E_0 $')
    plt.title(rf'$ E_0 $ con viriale, $ \nu $ = {nus[i]}')
    # plt.savefig(f'e0/vir-{nus[i]}.pdf')
    plt.show()
    plt.close()

der_E0_extrs = []
der_E0_extr_errs = []

for i in range(len(nus)):
    # faccio l'estrapolazione nel limite del continuo
    opt, cov = curve_fit(quadratic, etas[i], der_E0s[i], sigma=der_E0_errs[i], absolute_sigma=True, p0=[1, 0.8, 1])

    der_E0_extrs.append(opt[1])
    der_E0_extr_errs.append(np.sqrt(cov[1][1]))

    # grafici
    plt.plot([0, etas[i][-1]], 2*[E0_v[i]])

    plt.errorbar(np.array(etas[i]), der_E0s[i], yerr=der_E0_errs[i], label=nus[i])
    plt.errorbar([0], [opt[1]], yerr=[np.sqrt(cov[1][1])])
    X = np.linspace(0, max(etas[i]), 1000)
    plt.plot(X, quadratic(X, *opt))
    plt.xlabel(r'$ \eta $')
    plt.ylabel('$ E_0 $')
    plt.title(rf'$ E_0 $ con derivata di $ \log(Z) $, $ \nu $ = {nus[i]}')
    # plt.savefig(f'e0/der-{nus[i]}.pdf')
    plt.show()
    plt.close()

with open('E0s.txt', 'w') as outFile:
    for i in range(len(nus)):
        outFile.write(f'{nus[i]} {vir_E0_extrs[i]} {vir_E0_extr_errs[i]} {der_E0_extrs[i]} {der_E0_extr_errs[i]}\n')
