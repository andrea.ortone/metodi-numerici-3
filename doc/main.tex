\documentclass[a4paper]{article}
\usepackage{style}
\usepackage{fullpage}
\usepackage[separate-uncertainty=true]{siunitx}
\usepackage{subfig}

\title{
  Metodi numerici per la fisica \\
  Studio del potenziale a doppia buca \\
  \textsc{\large Progetto modulo 3}
}

\author{Andrea Ortone \and Marco Venuti}

\begin{document}
\maketitle
\tableofcontents

\section{Richiami teorici}
Lo scopo di questa analisi è lo studio dei primi livelli energetici quantistici di singola particella in un potenziale quartico \emph{a doppia buca}. In particolare ci concentreremo sulla
\begin{itemize}
\item determinazione dell'energia dello stato fondamentale che affronteremo con due diversi metodi;
\item stima del gap energetico tra stato fondamentale e primo eccitato e confronto tentativo con il comportamento atteso da approssimazione \emph{WKB}.
\end{itemize}
Nel corso di tutta l'analisi si considererà il potenziale:
\[ V(x) = b(x^{2} - a^{2})^{2} \]
e le seguenti scale caratteristiche:
\[
    L_c = a \qquad
    t_c = \frac{1}{a}\sqrt{\frac{m}{b}} \qquad
    E_c = \frac{\hbar}{t_c} = \hbar a \sqrt{\frac{b}{m}}
\]
L'azione euclidea discretizzata è
\[
    \frac{S_{E}}{\hbar} = \frac{\delta \tau}{\hbar} \sum_{j=1}^{N}
    \sbr{ \frac{m}{2} \del{x_{j+1}-x_{j}}^{2} + b(x_{j}^{2} - a^{2})^{2} }, \qquad \delta\tau = \frac{\beta \hbar}{N}
\]
dove \( x_{N+1} \equiv x_1 \). Riscalando le lunghezze secondo $y = \frac{x}{a}$ e definendo inoltre,
\[
    \nu = \frac{ma^{2}}{\hbar t_c} = \frac{\sqrt{mb} a^{3}}{\hbar} \qquad \eta = \frac{\delta \tau}{t_c}
\]
abbiamo dunque:
\[
    \frac{S_{E}}{\hbar} = \sum_{j=1}^{N} \nu \sbr{ \frac{1}{2\eta} (y_{j+1} - y_{j})^{2} + \eta (y_{j}^{2} - 1)^{2} } \qquad
    \frac{V}{E_c} = \nu(y^2-1)^2 \qquad
    \eta N = \tilde\beta
\]
dove \( \tilde\beta = \beta E_{c} \) è la temperatura adimensionale\footnote{
  Nelle sezioni relative alle simulazioni indicheremo le grandezze adimensionali semplicemente con il nome delle corrispondenti grandezze fisiche.
}. Secondo il formalismo del path-integral infine
\[
    \mathcal{Z}_\beta = \lim_{\eta \to 0} \left( \frac{\nu}{2\pi\eta} \right)^{N/2} \int_{y(0)=y(\tilde \beta)} \text{D}[y(\tau)] \ e^{-S_E/\hbar}
\]

\subsection{Calcolo dell'energia dello stato fondamentale}
\subsubsection{Teorema del viriale}
Sappiamo che sugli autostati dell'hamiltoniana
\[
    2\braket{T} = \braket{xV'(x)}
\]
Nel nostro caso
\[
    \braket{xV'(x)} = 4 b a^4 \braket{y^2 (y^2-1)}
\]
Da cui si ricava, in termini delle variabili adimensionali
\[
    \braket{H} = \braket{V} + \frac{1}{2} \braket{xV'(x)} = E_c \nu \braket{(y^2-1)(3y^2-1)}
\]
Nel limite di bassa temperatura la media termodinamica si riduce alla media sullo stato fondamentale e quindi possiamo ottenere una stima di \( E_0 \) tramite media con il path-integral
\begin{equation}\label{eq:viriale}
    \frac{E_0}{E_c} \simeq \nu \braket{(y^2-1)(3y^2-1)}\ped{PI} \qquad \text{ per } \beta \to +\infty
\end{equation}

\subsubsection{Derivata di \( \log \mathcal{Z} \)}
Partendo dalla relazione
\[
    \braket{H}_\beta = - \dpd{}{\beta} \log \mathcal{Z}
\]
si ricava, col formalismo del path-integral, nel limite di bassa temperatura
\begin{equation}\label{eq:derLogZ}
    \frac{E_0}{E_c} \simeq  \lim_{\eta \to 0}\  \nu \sbr{- \frac{1}{2\eta^2} \braket{(y_{j+1} - y_j)^2}\ped{PI} + \braket{(y_j^2-1)^2}\ped{PI}} + \frac{1}{2\eta}
\end{equation}

\subsection{Calcolo del gap \( E_1 - E_0 \)}
È possibile ottenere una stima del gap energetico tra stato fondamentale e primo eccitato usando la relazione, nel limite di \( \beta \) e \( \tau \) grandi,
\[
    \braket{x(\tau)x(0)}_{\beta} - \braket{x(0)}^2_{\beta} \simeq e^{-\tau \Delta_{10}/\hbar} \abs{\braket{0 | x | 1}}^2 + e^{-\tau \Delta_{30}/\hbar} \abs{\braket{0 | x | 3}}^2
\]
in unità adimensionali dunque
\[
    \braket{y(\tau)y(0)}_{\beta} - \braket{y(0)}^2_{\beta} \simeq e^{- \tilde\tau \tilde\Delta_{10}} \abs{\braket{0 | y | 1}}^2 + e^{-\tilde\tau \tilde \Delta_{30}} \abs{\braket{0 | y | 3}}^2
\]
ovvero, passando al formalismo del path-integral
\begin{equation}\label{eq:gap-en}
\begin{split}
  \braket{y_k y_0}\ped{PI} - \braket{y_0}^2\ped{PI} &\simeq
                                                      e^{-k \eta \tilde\Delta_{10}} \abs{\braket{0 | y | 1}}^2 + e^{-k \eta \tilde \Delta_{30}} \abs{\braket{0 | y | 3}}^2 \\
                                                    &= e^{-k \eta \tilde\Delta_{10}} \abs{\braket{0 | y | 1}}^2 \sbr{ 1 + \frac{ \abs{\braket{0 | y | 3}}^2 }{\abs{\braket{0 | y | 1}}^2} e^{-k \eta \tilde\Delta_{31} }}
\end{split}
\end{equation}

\subsubsection{Previsioni in approssimazione \emph{WKB}}\label{subsec:wkb}
Utilizzando l'approssimazione \emph{WKB}, valida nel limite di $\nu \gg 1$, il gap energetico è dato da
\[
    \Delta\ped{10} = \frac{\hbar \omega}{\pi} \exp\del{-\frac{1}{\hbar} \int_{-a_0}^{a_0} \sqrt{2m (V(x) - E_0)} \dif{x}}
\]
dove:
\begin{itemize}
\item $E_0$ è l'energia del fondamentale della singola buca;
\item $\omega$ è la frequenza classica di oscillazione sulla singola buca all'energia $E_0$;
\item $a_0$ è la soluzione più piccola e positiva dell'equazione $V(x) = E_0$
\end{itemize}
Si potrebbe procedere calcolando $E_0$ e quindi $a_0$ e $\omega$ sempre in approssimazione \emph{WKB}. Per ottenere una stima più rozza per $\Delta\ped{10} $ possiamo utilizzare l'approssimazione quadratica del potenziale attorno al minimo (la correzione a \( E_{0} \) tende a zero nel limite \( \nu\to +\infty \)):
\[
    V(x) \simeq 4ba^2(x-a)^2 \qquad \Rightarrow \qquad \omega = 2\sqrt{2}a\sqrt{\frac{b}{m}}
\]
Ottenendo dunque
\[
    E_0 = \frac{\hbar \omega}{2} \qquad a_0 = \sqrt{ a^2 - \sqrt{\frac{\hbar \omega}{2b}}}
\]
e infine, tornando a unità adimensionali:
\[
    \tilde\Delta\ped{10} = \frac{2\sqrt{2}}{\pi} \exp{\del{-2\sqrt{2} \nu \int_{0}^{\sqrt{1- (2/\nu^2)^{1/4}}} \sqrt{(t^2-1)^2 - \sqrt{2}/\nu}\ \dif{t}}}
\]
Come prevedibile il gap energetico decresce molto rapidamente con $\nu$: per esempio se $\nu \simeq 5$ si avrebbe $\tilde\Delta\ped{10} \simeq 10^{-3}$. Ricordando l'equazione~\eqref{eq:gap-en}, si vede che il $\bar k$ caratteristico con cui decade la funzione di correlazione nel tempo euclideo è pari a
\begin{equation}\label{eq:kbar}
    \bar k = \frac{1}{\eta \tilde\Delta\ped{10}}
\end{equation}
che per $\eta \simeq 0.1$ determina $\bar k \simeq 10^{4}$.
Questo ci costringerebbe a scegliere tempi euclidei (ovvero $\beta$) molto elevati e dunque a fare simulazioni computazionalmente molto costose.
Per questo motivo abbiamo deciso di limitarci a valori di $\nu$ meno elevati e, invece di  confrontare i risultati ottenuti dalle simulazioni con le predizioni della \emph{WKB}, abbiamo deciso di calcolare le energie del sistema diagonalizzando numericamente l'hamiltoniana del sistema discretizzato (si veda \texttt{schrodinger.py}). \\
Per completezza, in Figura~\ref{fig:wkb} è possibile osservare come i risultati della \emph{WKB} diventino compatibili con quelli dati dalla diagonalizzazione solo per valori di $\nu$ \emph{troppo} elevati.

\begin{figure}[h!]
    \centering
    \subfloat[Gap.]{\includegraphics[scale=0.53]{fig/wkb}}
    \subfloat[Errore relativo tra le due predizioni.]{\includegraphics[scale=0.53]{fig/wkb-err}}
    \caption{\label{fig:wkb}confronto tra la predizione della \emph{WKB} e la diagonalizzazione al variare di \( \nu \).}
\end{figure}


\subsection{Forma del fondamentale}

Possiamo ottenere il modulo quadro della funzione d'onda dello stato fondamentale ricordando che, per \( \beta \to +\infty \),
\[
    \braket{\delta(y-x(0))}\ped{PI} = \frac{1}{\mathcal Z}\tr[e^{-\beta \hat{H}} \delta(y-x)] \simeq \mean{0 | \delta(y-x) | 0} = \abs{\psi_{0}(y)}^{2}
\]
Nella pratica la \( \delta \) va regolarizzata; una possibilità è quella di prendere semplicemente un istogramma della posizione dei siti del cammino normalizzata con il numero totale di misure (sia al variare del tempo euclideo che Monte Carlo), in quanto questa procedura è equivalente a prendere come regolarizzazione della \( \delta \) una funzione caratteristica di opportuni intervalli (quelli tra il massimo e il minimo di \( y \) suddivisi in maniera regolare).

\section{Analisi delle simulazioni}
Abbiamo innanzi tutto cercato un valore di \( \beta \) abbastanza elevato per cui le osservabili di interesse raggiungessero un \emph{plateau} al variare di \( \beta \) stesso.
In particolare, abbiamo fissato \( \nu = 0.5 \) e fatto simulazioni per vari \( \beta \), estrapolando i valori delle osservabili nel limite del continuo.

In Figura~\ref{fig:sweep-beta} si può vedere che effettivamente per \( \beta \gtrsim 60 \) le stime delle osservabili si sono stabilizzate entro le loro incertezze.

\begin{figure}[h!]
    \centering
    \subfloat[Energia del fondamentale calcolata in due modi diversi, come descritto nella sezione teorica.]{\includegraphics[scale=0.53]{fig/e0-beta}}
    \subfloat[Gap tra il fondamentale e il primo eccitato.]{\includegraphics[scale=0.53]{fig/gap-beta}}
    \caption{\label{fig:sweep-beta}sweep in temperatura per l'energia del fondamentale e per il primo gap. In entrambi i casi \( \nu = 0.5 \).}
\end{figure}

Tenendo conto di quanto detto, abbiamo fissato \( \beta = 100 \) e fatto simulazioni, in accordo con quanto discusso nella sezione~\ref{subsec:wkb}, per
\[
    \nu = 0.4,\ 0.45,\ 0.5,\ 0.55,\ 0.6,\ 1.0
\]
Per ogni \( \nu \) abbiamo eseguito la simulazione per diversi valori di \( \eta \) (visibili ad esempio in Figura~\ref{fig:e0}) con \( 10^{7}/\eta \) sweep, misurando ogni \( 10^{3}/\eta \) sweep. La scelta di prendere simulazioni di lunghezza \( \propto 1/\eta \) è dovuta al fatto che ci aspettiamo che il tempo caratteristico di correlazione nel tempo Monte Carlo aumenti con il diminuire di \( \eta \). \\
Per i dettagli implementativi si veda \texttt{cammini\_gen.cpp}.

\subsection{Energia del fondamentale}
Procedendo come indicato nei richiami teorici, per ogni \( \nu \) ed \( \eta \) abbiamo calcolato\footnote{
  Si vedano \texttt{energia.cpp} e \texttt{energy\_analysis.py}.
} il valore delle osservabili~\eqref{eq:viriale} e~\eqref{eq:derLogZ}.
Grazie all'invarianza per traslazione nel tempo euclideo, possiamo migliorare la precisione dei valori centrali facendo la media, oltre che nel tempo Monte Carlo, anche nel tempo euclideo. Tuttavia, per calcolare gli errori occorre tenere conto delle correlazioni sia nel tempo Monte Carlo che nel tempo euclideo. Per fare questo abbiamo calcolato una stima dell'osservabile separatamente per ogni tempo euclideo (mediando solo sul tempo Monte Carlo) e quindi applicato la procedura di \emph{blocking} sulle \( N \) misure così ottenute fino al raggiungimento di un \emph{plateau} nell'errore.

Ottenute queste stime, abbiamo quindi estrapolato il limite al continuo delle osservabili facendo un fit con correzioni quadratiche e quartiche in \( \eta \). Nella seguente tabella si riportano i valori così ottenuti (nelle ultime due colonne), assieme al valore ottenuto dalla diagonalizzazione dell'hamiltoniana:
\begin{center}
    \begin{tabular}{cccc} \toprule
      \( \nu \) & diag  & viriale              & der \( \log Z \) \\ \midrule
      0.4       & 0.813 & \num{0.812 +- 0.001} & \num{0.814 +- 0.002} \\
      0.45      & 0.801 & \num{0.800 +- 0.001} & \num{0.797 +- 0.002} \\
      0.5       & 0.794 & \num{0.794 +- 0.001} & \num{0.791 +- 0.002} \\
      0.55      & 0.792 & \num{0.792 +- 0.001} & \num{0.793 +- 0.002} \\
      0.6       & 0.793 & \num{0.792 +- 0.001} & \num{0.794 +- 0.002} \\
      1.0       & 0.870 & \num{0.870 +- 0.001} & \num{0.872 +- 0.002} \\ \bottomrule
    \end{tabular}
\end{center}
In Figura~\ref{fig:e0} sono riportati due grafici esemplificativi del fit per il limite al continuo relativi a \( \nu = 1 \), mentre in Figura~\ref{fig:schroed-e0} sono riassunti i risultati ottenuti.

\begin{figure}[h!]
    \centering
    \subfloat[Calcolata usando la~\eqref{eq:viriale}]{\includegraphics[scale=0.53]{fig/e0/vir-1.0}}
    \subfloat[Calcolata usando la~\eqref{eq:derLogZ}]{\includegraphics[scale=0.53]{fig/e0/der-1.0}}
    \caption{\label{fig:e0}estrapolazione dell'energia del fondamentale nel limite del continuo \( \eta \to 0 \), stimata in due modi diversi, per un particolare valore di \( \nu \). La retta orizzontale è il valore dato dalla diagonalizzazione.}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{fig/schroed-e0}
    \caption{\label{fig:schroed-e0}confronto dell'energia del fondamentale al variare di \( \nu \) come ottenuta dal Monte Carlo e dalla diagonalizzazione (Schrödinger).}
\end{figure}
\newpage

\subsection{Gap \( E_{1} - E_{0} \)}
Per gli stessi \( \nu \) ed \( \eta \) della sezione precedente abbiamo calcolato\footnote{
    Si veda \texttt{corr.cpp}.
} i correlatori nel tempo euclideo dapprima separatamente per ogni tempo euclideo (mediando sul tempo Monte Carlo) e quindi mediando anche in quest'ultimo. Gli errori sono stati calcolati facendo \emph{blocking} nel tempo euclideo.

I valori della funzione di correlazione \( C(k) \) (separatamente per ogni \( \nu \) ed \( \eta \)) così ottenuti sono stati fittati con un'esponenziale per ottenere il gap secondo la~\eqref{eq:kbar}.

Nel fare i fit abbiamo messo un cutoff dal basso su \( k \), derivante dal fatto che nella~\eqref{eq:gap-en} deve essere
\[
    e^{-k \eta \tilde\Delta_{13}} \ll 1 \qquad \Rightarrow \qquad k \gtrsim \frac{1}{\eta \tilde\Delta_{13}}
\]
per garantire che il contributo dei gap superiori alla funzione di correlazione sia trascurabile. Il gap 1-3 è stato assunto rozzamente \( \sim 5 \) per ogni \( \nu \) (anche sulla base di quanto ricavato dalla diagonalizazione).

Notiamo però che stiamo eseguendo un fit su dati correlati (in quanto provenienti dalla stessa simulazione). Questo fa sì che l'errore sui parametri restituti dal fit (che assume dati scorrelati) siano sottostimati\footnote{
  La conseguenza di questa sottostima è che i punti con \( \eta \) più piccolo nei fit visibili in Figura~\ref{fig:gap} condizionano pesantemente il valore dell'estrapolazione nel limite del continuo, essendo tali punti appunto i più vicini all'intercetta.
}. Per ridurre tale correlazione, abbiamo quindi deciso, per i fit relativi agli \( \eta \) più piccoli (che sono quelli con più dati) di escludere alcuni dati intermedi\footnote{
  Per maggiori dettagli si veda \texttt{corr\_analysis.py}.
}.

A partire dalle stime dei gap così ottenute, in modo analogo a quanto visto nella sezione precedente, per ogni \( \nu \) abbiamo eseguito un fit (con correzioni quadratiche e quartiche in \( \eta \)) per estrapolare il limite al continuo dei gap stessi.
I valori ottenuti, visibili anche in Figura~\ref{fig:gap}, sono qui riportati:
\begin{center}
    \begin{tabular}{ccc} \toprule
      \( \nu \) & diag  & Monte Carlo            \\ \midrule
      0.4       & 1.675 & \num{1.675 +- 0.004}   \\
      0.45      & 1.555 & \num{1.554 +- 0.004}   \\
      0.5       & 1.450 & \num{1.448 +- 0.003}   \\
      0.55      & 1.356 & \num{1.353 +- 0.003}   \\
      0.6       & 1.271 & \num{1.270 +- 0.003}   \\
      1.0       & 0.792 & \num{0.791 +- 0.001}   \\ \bottomrule
    \end{tabular}
\end{center}
I risultati sono compatibili con quanto ricavato tramite la diagonalizzazione, come visibile anche in Figura~\ref{fig:schroed-gap}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7]{fig/schroed-gap}
    \caption{\label{fig:schroed-gap}confronto del gap \( \Delta_{10} \) al variare di \( \nu \) come ottenuto dal Monte Carlo e dalla diagonalizzazione.}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7]{fig/ck}
    \caption{esempio di correlatore nel tempo euclideo. I dati segnati con delle croci blu sono quelli effettivamente usati nel fit.}
\end{figure}

\begin{figure}[h!]
    \centering
    \subfloat{\includegraphics[scale=0.53]{fig/gap/0.4}}
    \subfloat{\includegraphics[scale=0.53]{fig/gap/0.45}} \\
    \subfloat{\includegraphics[scale=0.53]{fig/gap/0.5}}
    \subfloat{\includegraphics[scale=0.53]{fig/gap/0.55}} \\
    \subfloat{\includegraphics[scale=0.53]{fig/gap/0.6}}
    \subfloat{\includegraphics[scale=0.53]{fig/gap/1.0}}
    \caption{\label{fig:gap}estrapolazione del gap tra fondamentale e primo eccitato nel limite del continuo \( \eta \to 0 \) per i valori di \( \nu \) simulati.}
\end{figure}
\clearpage

\subsection{Forma del fondamentale}
Procedendo come descritto nei richiami teorici, riportiamo in Figura~\ref{fig:hist-1} e~\ref{fig:hist-2} due istogrammi esemplificativi ottenuti dalle simulazioni. Notiamo che per \( \nu = 1 \) la funzione d'onda comincia a localizzarsi di più attorno ai minimi del potenziale (che sono in \( \pm 1 \)) ed è correttamente pari (se fosse dispari il modulo quadro sarebbe nullo in zero).
Notiamo inoltre che
\begin{itemize}
\item Volendo attribuire degli errori ai valori dati dall'istogramma, ovvero i \( \abs{\psi(x_{j})}^{2} \), si potrebbe procedere come fatto per le altre osservabili, ovvero calcolando ciascuna quantità separatamente per ogni tempo euclideo, e poi eseguendo blocking sui dati così ottenuti.
\item Anche per queste osservabili bisognerebbe procedere estrapolando il limite al continuo per ogni \( j \). In Figura~\ref{fig:hist-eta} è riporato l'andamento di uno degli istogrammi al variare di \( \eta \).
\end{itemize}

\begin{figure}[h!]
    \centering
    \subfloat[\label{fig:hist-1}\( \eta = 0.05 \)]{\includegraphics[scale=0.53]{fig/wavefunction-0.4.pdf}}
    \subfloat[\label{fig:hist-2}\( \eta = 0.05 \)]{\includegraphics[scale=0.53]{fig/wavefunction-1.0.pdf}}\\
    \subfloat[\label{fig:hist-eta}Per \( \nu = 1 \) e al variare di \( \eta \).]{\includegraphics[scale=0.53]{fig/wavefunction-etas.pdf}}
    \caption{\label{fig:hist}modulo quadro della funzione d'onda del fondamentale \( a = 1 \).}
\end{figure}


\end{document}
